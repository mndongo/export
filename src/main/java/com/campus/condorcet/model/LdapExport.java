package com.campus.condorcet.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
// on prod use schema : test_sch
//@Table(name = "ldapExport")
@Table(name = "test_sch.ldapExport")
public class LdapExport {

    
	//private Integer id ;
	@Id
    @Column(name = "UID", nullable = false)
	private String uid;
	
	@Column(name="sn")
	private String sn;
	
	@Column(name="givenname")
	private String givenname;
	
	@Column(name="cn")
	private String cn;
	
	@Column(name="mail")
	private String mail;
	
	@Column(name="postalAddress")
	private String postalAddress;
	
	@Column(name="supannentiteAffectation")
	private String supannentiteaffectation;
	
	@Column(name="supannentiteaffectationprincipale")
	private String supannentiteaffectationprincipale;
	
	@Column(name="telephonenumber")
	private String telephonenumber ;
	
	@Column(name="createTimestamp")
	private String createTimestamp;
	
	@Column(name="displayName")
	private String displayName;
	
	@Column(name="eduPersonAffiliation")
	private String eduPersonAffiliation;
	
	@Column(name="eduPersonPrimaryAffiliation")
	private String eduPersonPrimaryAffiliation;
	
	@Column(name="eduPersonPrincipalName")
	private String eduPersonPrincipalName;
	
	@Column(name="employeeNumber")
	private String employeeNumber;
	
	@Column(name="fdBadge")
	private String fdBadge;
	
	@Column(name="fdContractEndDate")
	private String fdContractEndDate;
	
	@Column(name="fdContractStartDate")
	private String fdContractStartDate;
	
	@Column(name="l")
	private String l;
	
	@Column(name="mobile")
	private String mobile;
	
	@Column(name="modifyTimestamp")
	private String modifyTimestamp;
	
	@Column(name="o")
	private String o;
	
	@Column(name="objectClass")
	private String objectClass;
	
	@Column(name="ou")
	private String ou;
	
	@Column(name="personalTitle")
	private String personalTitle;
	
	@Column(name="roomNumber")
	private String roomNumber;
	
	@Column(name="supannEtablissement")
	private String supannEtablissement;
	
	@Column(name="supannMailPerso")
	private String supannMailPerso;
	
	@Column(name="supannRefId")
	private String supannRefId;
	
	@Column(name="title")
	private String title ;
	
	@Column(name="statusdate")
	private String statusDate;
	
	@Column(name="recordType")
	private String recordType;
	
	@Column(name="userGroup")
	private String userGroup;
	
	@Column(name="statisticCategory")
	private String statisticCategory;
	
	@Column(name="categoryType")
	private String categoryType;
	
	@Column(name="userGroupDesc")
	private String userGroupDesc;
	
	//TODO V1.1 ajout d'une colonne supplémenataire dans la base et dans le ficihier de config lsc : supannEtuTypeDiplome
	@Column(name="supannEtuTypeDiplome")
	private String supannEtuTypeDiplome;
	
	@Column(name="compteenddate")
	private String compteEndDate;
	
	/*public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}*/
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getSn() {
		return sn;
	}
	public void setSn(String sn) {
		this.sn = sn;
	}
	public String getGivenname() {
		return givenname;
	}
	public void setGivenname(String givenname) {
		this.givenname = givenname;
	}
	public String getCn() {
		return cn;
	}
	public void setCn(String cn) {
		this.cn = cn;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getPostalAddress() {
		return postalAddress;
	}
	public void setPostalAddress(String postalAddress) {
		this.postalAddress = postalAddress;
	}
	public String getSupannentiteaffectation() {
		return supannentiteaffectation;
	}
	public void setSupannentiteaffectation(String supannentiteaffectation) {
		this.supannentiteaffectation = supannentiteaffectation;
	}
	public String getSupannentiteaffectationprincipale() {
		return supannentiteaffectationprincipale;
	}
	public void setSupannentiteaffectationprincipale(String supannentiteaffectationprincipale) {
		this.supannentiteaffectationprincipale = supannentiteaffectationprincipale;
	}
	public String getTelephonenumber() {
		return telephonenumber;
	}
	public void setTelephonenumber(String telephonenumber) {
		this.telephonenumber = telephonenumber;
	}
	public String getCreateTimestamp() {
		return createTimestamp;
	}
	public void setCreateTimestamp(String createTimestamp) {
		this.createTimestamp = createTimestamp;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public String getEduPersonAffiliation() {
		return eduPersonAffiliation;
	}
	public void setEduPersonAffiliation(String eduPersonAffiliation) {
		this.eduPersonAffiliation = eduPersonAffiliation;
	}
	public String getEduPersonPrimaryAffiliation() {
		return eduPersonPrimaryAffiliation;
	}
	public void setEduPersonPrimaryAffiliation(String eduPersonPrimaryAffiliation) {
		this.eduPersonPrimaryAffiliation = eduPersonPrimaryAffiliation;
	}
	public String getEduPersonPrincipalName() {
		return eduPersonPrincipalName;
	}
	public void setEduPersonPrincipalName(String eduPersonPrincipalName) {
		this.eduPersonPrincipalName = eduPersonPrincipalName;
	}
	public String getEmployeeNumber() {
		return employeeNumber;
	}
	public void setEmployeeNumber(String employeeNumber) {
		this.employeeNumber = employeeNumber;
	}
	public String getFdBadge() {
		return fdBadge;
	}
	public void setFdBadge(String fdBadge) {
		this.fdBadge = fdBadge;
	}
	public String getFdContractEndDate() {
		return fdContractEndDate;
	}
	public void setFdContractEndDate(String fdContractEndDate) {
		this.fdContractEndDate = fdContractEndDate;
	}
	public String getFdContractStartDate() {
		return fdContractStartDate;
	}
	public void setFdContractStartDate(String fdContractStartDate) {
		this.fdContractStartDate = fdContractStartDate;
	}
	public String getL() {
		return l;
	}
	public void setL(String l) {
		this.l = l;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getModifyTimestamp() {
		return modifyTimestamp;
	}
	public void setModifyTimestamp(String modifyTimestamp) {
		this.modifyTimestamp = modifyTimestamp;
	}
	public String getO() {
		return o;
	}
	public void setO(String o) {
		this.o = o;
	}
	public String getObjectClass() {
		return objectClass;
	}
	public void setObjectClass(String objectClass) {
		this.objectClass = objectClass;
	}
	public String getOu() {
		return ou;
	}
	public void setOu(String ou) {
		this.ou = ou;
	}
	public String getPersonalTitle() {
		return personalTitle;
	}
	public void setPersonalTitle(String personalTitle) {
		this.personalTitle = personalTitle;
	}
	public String getRoomNumber() {
		return roomNumber;
	}
	public void setRoomNumber(String roomNumber) {
		this.roomNumber = roomNumber;
	}
	public String getSupannEtablissement() {
		return supannEtablissement;
	}
	public void setSupannEtablissement(String supannEtablissement) {
		this.supannEtablissement = supannEtablissement;
	}
	public String getSupannMailPerso() {
		return supannMailPerso;
	}
	public void setSupannMailPerso(String supannMailPerso) {
		this.supannMailPerso = supannMailPerso;
	}
	public String getSupannRefId() {
		return supannRefId;
	}
	public void setSupannRefId(String supannRefId) {
		this.supannRefId = supannRefId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	/*
	public String getStatusDate() {
		return statusDate;
	}
	public void setStatusDate(String statusDate) {
		this.statusDate = statusDate;
	}*/
	public String getStatusDate() {
		return statusDate;
	}
	public void setStatusDate(String statusDate) {
		this.statusDate = statusDate;
	}
	public String getRecordType() {
		return recordType;
	}
	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}
	public String getUserGroup() {
		return userGroup;
	}
	public void setUserGroup(String userGroup) {
		this.userGroup = userGroup;
	}
	public String getStatisticCategory() {
		return statisticCategory;
	}
	public void setStatisticCategory(String statisticCategory) {
		this.statisticCategory = statisticCategory;
	}
	public String getCategoryType() {
		return categoryType;
	}
	public void setCategoryType(String categoryType) {
		this.categoryType = categoryType;
	}
	public String getUserGroupDesc() {
		return userGroupDesc;
	}
	public void setUserGroupDesc(String userGroupDesc) {
		this.userGroupDesc = userGroupDesc;
	}
	public String getSupannEtuTypeDiplome() {
		return supannEtuTypeDiplome;
	}
	public void setSupannEtuTypeDiplome(String supannEtuTypeDiplome) {
		this.supannEtuTypeDiplome = supannEtuTypeDiplome;
	}
	public String getCompteEndDate() {
		return compteEndDate;
	}
	public void setCompteEndDate(String compteEndDate) {
		this.compteEndDate = compteEndDate;
	}
}
