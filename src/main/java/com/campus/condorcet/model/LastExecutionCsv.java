package com.campus.condorcet.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "lastexecutionCsv")
//@Table(name="test_sch.lastexecutioncsv")
public class LastExecutionCsv {
	@Id
    @Column(name = "ID", nullable = false)
	private String Id;
	
	@Column(name="lastdateexecution")
	private Date lastdateexecution;

	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}

	public Date getLastdateexecution() {
		return lastdateexecution;
	}

	public void setLastdateexecution(Date lastdateexecution) {
		this.lastdateexecution = lastdateexecution;
	}
}
