package com.campus.condorcet.model;

public class FtpInfo {

	private String ftpHost;
	private String ftpUserId;
	private String ftpPassword;
	private String ftpFolderLocation;
	private String ftpPort;
	
	public String getFtpHost() {
		return ftpHost;
	}
	public void setFtpHost(String ftpHost) {
		this.ftpHost = ftpHost;
	}
	public String getFtpUserId() {
		return ftpUserId;
	}
	public void setFtpUserId(String ftpUserId) {
		this.ftpUserId = ftpUserId;
	}
	public String getFtpPassword() {
		return ftpPassword;
	}
	public void setFtpPassword(String ftpPassword) {
		this.ftpPassword = ftpPassword;
	}
	public String getFtpFolderLocation() {
		return ftpFolderLocation;
	}
	public void setFtpFolderLocation(String ftpFolderLocation) {
		this.ftpFolderLocation = ftpFolderLocation;
	}
	public String getFtpPort() {
		return ftpPort;
	}
	public void setFtpPort(String ftpPort) {
		this.ftpPort = ftpPort;
	}
	
}
