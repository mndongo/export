package com.campus.condorcet.dao;

import java.util.List;

import com.campus.condorcet.model.User;

public interface IUserDao extends IReferentielIdentiteDao<User>{

	public User loadUserByUsername(String username);

	public List<User> loadUser();
}
