package com.campus.condorcet.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.campus.condorcet.model.CsvDataExport;
import com.campus.condorcet.model.LastExecutionCsv;
import com.campus.condorcet.model.LdapExport;

public interface IControleAccesServiceDao extends IReferentielIdentiteDao<CsvDataExport>{

	public LastExecutionCsv getLastExecution();
	public HashMap<String,String> loadCsvDataExport();
	public void save(CsvDataExport csvDataExport) ;
	public List<LdapExport> loadAll();
	public List<CsvDataExport> getallUserExported();
		
	
}
