package com.campus.condorcet.service;

import java.util.List;
import java.util.Properties;

import org.springframework.transaction.annotation.Transactional;

import com.campus.condorcet.model.LdapExport;

public interface ILdapExportService {

	@Transactional
	public List<LdapExport> loadReferentielUser(Properties properties,String dateModif,String op);
}
