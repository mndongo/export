package com.campus.condorcet.service.impl;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import org.apache.log4j.Logger;

import com.campus.condorcet.model.LdapExport;

import sun.util.logging.resources.logging;

public class TraitementUtil {

	private static final Logger logger = Logger.getLogger(TraitementUtil.class);
	/**
	 * supprime le 1er et le dernier crochet dans la chaine de caractere
	 * @param user
	 * @return
	 */
	public static StringBuilder deleteFromSupannentiteaffectation(LdapExport user) {
		StringBuilder buf = new StringBuilder(user.getSupannentiteaffectation());
		getString(buf);
		return buf;
	}
	
	public static StringBuilder VerifyAffectation(LdapExport user) {
		StringBuilder buf = new StringBuilder(user.getSupannentiteaffectation());
		getString(buf);
		return buf;
	}
	
	public static List<String> getStringToList(StringBuilder buf) {
		String[] affectationListe = buf.toString().trim().split(",");
		List<String> lArray = Arrays.asList(affectationListe);
		return lArray;
	}
	
	public static LdapExport deleteCrochetForUser(LdapExport user) {
		StringBuilder buf = new StringBuilder(user.getSupannentiteaffectation());
		StringBuilder bufAff = new StringBuilder(user.getEduPersonAffiliation());
		buf = getString(buf);
		bufAff = getString(bufAff);
		user.setSupannentiteaffectation(buf.toString());
		user.setEduPersonAffiliation(bufAff.toString());
		return user;
	}

	public static StringBuilder getString(StringBuilder buf) {
		buf.delete(0, 1);
		buf.deleteCharAt(buf.length()-1);
		return buf;
	}
	
	/**
	 * 
	 * @return
	 */
	public static StringBuilder getdateDuJourFormatAlma() {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		String statusDate = format.format(cal.getTime());
		StringBuilder sb = new StringBuilder(statusDate) ;
		sb.append("-Z");
		return sb;
	}
	
	public static String getdateDuJourFormat() {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		String statusDate = format.format(cal.getTime());
		StringBuilder sb = new StringBuilder(statusDate) ;
		//sb.append("-Z");
		return statusDate;
	}
	
	/**
	 * 
	 * @param user
	 */
	public static String  getExpiryDate(LdapExport user) {
		String dateFdcontract = "";
		if(user.getFdContractEndDate() != null) {
			user.setFdContractEndDate(user.getFdContractEndDate().replaceFirst("00Z", "Z"));
			dateFdcontract = getFormatXmlDate(user.getFdContractEndDate());
		}
		return dateFdcontract;
	}
	
	/**
	 * transforme la date issue du ldap
	 * 
	 */
	public static String getFormatXmlDate(String date) {
		StringBuilder builder  = new StringBuilder(date).insert(4, "-").insert(7, "-").insert(10, "-");
		return builder.toString();
	}
	
}
