package com.campus.condorcet.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.file.FileAlreadyExistsException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.CharEncoding;
import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.exception.MethodInvocationException;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.campus.condorcet.dao.ILdapExportInterfaceDao;
import com.campus.condorcet.exception.SuppanAffectationException;
import com.campus.condorcet.model.LdapExport;
import com.campus.condorcet.service.ILdapExportService;
import com.campus.condorcet.service.IRefIndentiteUploadService;
import com.campus.condorcet.utils.RefIdConstants;

import sun.net.ftp.FtpLoginException;
@Service("ldapService")
public abstract class LdapExportService implements ILdapExportService{

	public abstract void createTemplateType(List<LdapExport> lUtilisateur);
	public abstract void loadTemplateT(Template template, VelocityContext context); 
	 
	private static final Logger logger = Logger.getLogger(LdapExportService.class);
	@Autowired
	ILdapExportInterfaceDao ldapdao;
	
	@Autowired 
	IRefIndentiteUploadService refIdentiteUploadService;
	
	@Value("${ftp.ftpuserId}")
	protected String user;
	
	@Value("${ftp.ftpPassword}")
	protected String password;
	
	@Value("${ftp.folderLocation}")
	protected String folderLocation;
	
	@Value("${ftp.ftpPort}")
	protected String port;
	
	
	@Value("${jdbc.url}")
	protected String url;
	
	@Value("${jdbc.username}")
	protected String username;
	
	@Value("${jdbc.password}")
	protected String pwd;
	/*
	@Value("${ftp.folderOut}")
	private String folderOut;*/
	
	
	//protected int i=0;

	public void setLdapdao(ILdapExportInterfaceDao ldapdao) {
		this.ldapdao = ldapdao;
	}
	@Value("${ftp.ftpHost}")
	protected String host;


	@Override
	public List<LdapExport> loadReferentielUser(Properties properties, String dateModif, String op) {
		return null;
	}
	
	/**
	 * 
	 * @param properties
	 * @param lUtilisateur
	 * @param dateDuJour
	 * @param sUserNotHavingEmployerNumberOrGroup
	 */
	public void initUser(Properties properties, List<LdapExport> lUtilisateur, StringBuilder dateDuJour,
			Set<LdapExport> sUserNotHavingEmployerNumberOrGroup) {
		lUtilisateur.stream().forEach((user)->{
			// set la date de fin de contrat
			String dateForContract = TraitementUtil.getExpiryDate(user);
			user.setFdContractEndDate(dateForContract);
			// set la date du jour pour chaque utilisateur dans la template
			user.setStatusDate(dateDuJour.toString());
			
			/** mail perso, phones tests effectués depuis la template */
			// determine l'affectation et initialise le usergroup
			if(null != user.getEduPersonAffiliation() || !user.getEduPersonAffiliation().isEmpty()) {
				getAffiliation(user,properties);
			}
			
			if (null == user.getUserGroup() || user.getUserGroup().isEmpty() || user.getUserGroup() =="") {
				sUserNotHavingEmployerNumberOrGroup.add(user);
			}
			// utilisateur qui n'ont pas d'employeeNumber
			if(null == user.getEmployeeNumber() ||user.getEmployeeNumber().isEmpty() ) {
				sUserNotHavingEmployerNumberOrGroup.add(user);
			}
			// determine user identifier
			if(null != user.getFdBadge()) {
				user.setFdBadge(user.getFdBadge());
			}
			// determine user statistic category
			if(null != user.getO()) {
				user.setStatisticCategory(user.getO());
				//logger.info("static O  is : "+user.getO());
				user.setCategoryType(RefIdConstants.CATEG_ETABLISSEMENT);
			}else  if(null != user.getOu()) {
				user.setStatisticCategory(RefIdConstants.STAISTIC_CATEG_OU);
				//logger.info("static OU  is : "+user.getOu());
				user.setCategoryType(RefIdConstants.CATEG_LABORATOIRE);
			}else {
				user.setStatisticCategory("");
				user.setCategoryType("");
			}
			/*if(null != user.getL() || !user.getL().isEmpty()) {
				logger.info("****** L = "+user.getL()+" ******************");
			}*/
		
		});
		logger.error("*** "+sUserNotHavingEmployerNumberOrGroup.size()+ " n'ont pas de user group ou d'employer number et ne seront pas exportés ");
	}
	
	/**
	 * 
	 * @param user
	 * @param properties
	 */
	public void getAffiliation(LdapExport user,Properties properties) {
		
	}
	
	/**
	 * recupère le profile de l'utilisateur
	 * @param user
	 * @param properties
	 * @param lEdupersonAffiliationWithoutSpace
	 */
	public void getProfileUserAff(LdapExport user, Properties properties,List<String> lEdupersonAffiliationWithoutSpace) {
		
	}
	/**
	 * return les utilisateurs qui ne seront pas exportés
	 * @param lUtilisateur
	 * @param properties
	 * @return
	 * @throws SuppanAffectationException
	 */
	public Set<LdapExport> getUserNotExported(List<LdapExport> lUtilisateur,Properties properties) throws SuppanAffectationException {
		return null;
	}

	/**
	 * 
	 * @param lAffectation
	 * @param user
	 * @param properties
	 */
	public void getRecordTypeUser(List<String> lAffectation, LdapExport user,Properties properties) {
		String[] lProfilRecordType = properties.get("profil.record").toString().split(",");
		for(String valAffectation : lAffectation) {
			if(ArrayUtils.contains(lProfilRecordType, valAffectation.trim())) {
				user.setRecordType(properties.getProperty("profil.record.libelle.staff"));
				break;
			}else {
				user.setRecordType(properties.getProperty("profil.record.libelle.public"));
			}
		}
		
	}
	
	
	// zip le fichier
	
	public  File zipFile(File fileCreated) throws IOException {
		 ZipOutputStream out = null;
		try {
			
			// input file 
	        FileInputStream in = new FileInputStream(fileCreated);
	        // out put file 
	        out = new ZipOutputStream(new FileOutputStream(folderLocation+RefIdConstants.ZIP));
	        // name the file inside the zip  file 
	        out.putNextEntry(new ZipEntry(fileCreated.getName())); 
	        // buffer size
	        byte[] b = new byte[1024];
	        int count;
	        while ((count = in.read(b)) > 0) {
	            out.write(b, 0, count);
	        }
	        
	        File directory = new File(folderLocation);
	        File[] fList = directory.listFiles();
	        for(File f : fList) {
	        	if(RefIdConstants.ZIP.equals(f.getName())) {
	        		return f;
	        	}
	        }
	        
	        out.close();
	        in.close();
		}catch (Exception e) {
			 logger.error("Exception when trying to zip the logs: " + e.getMessage());
			 out.closeEntry();
		}
			
		return null;
		
	}
	
	/**
	 * construit une template pour tous les utilisateurs déjà exportés
	 * @param template
	 * @param context
	 */
	public void loadTemplateForAll(Template template, VelocityContext context) {
		
		
	}
	
}
