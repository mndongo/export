package com.campus.condorcet.utils;

public class RefIdConstants {

	public static final String REF_ID_EXPORT_VM_TEMPLATE = "refId_export.vm";
	
	public static final String FILE_RESOURCE_LOADER_PATH_TEMPLATE="src/main/resources/templates";
	
	public static final String FORMAT = "%1$tY-%1$tm-%1$td";

	/* DEV FILES CONFIG */
	//public static final String SRC_MAIN_RESOURCES_EXPORT_SOURCE_DIR_DEV ="src/main/resources/output/";
	//public static final String SRC_MAIN_RESOURCES_OUTPUT = "/opt/refIdExport/output/";
	public static final String SRC_MAIN_RESOURCES_PROPS = "src/main/resources/refeIdententite.properties/";
	//properties contenat la liste des etablissement entre autres
	public static final String SRC_MAIN_RESOURCES_OPT_PROPS = "/home/mndongo/refIdExport/listeEtablisement.properties/";
	
	/* FILES CONFIG */
	public static final String SRC_MAIN_RESOURCES_EXPORT_DIR = "home/exlibris/";
	// TODO à mettre dans le fichier properties
	public static final String SRC_MAIN_RESOURCES_EXPORT_REMOTE_DIR = "/SIS/sync/";
	
	
	public static final String STAFF_MEMEBER="D008";
	
	public static final String REGISTERED_READER="registered-reader"; 
	
	
	public static final String STUDENT="student"; 
	
	public static final String MASTER="master";
	public static final String PROFIL_CU1="CU1";
	public static final String PROFIL_CU2="CU2";
	public static final String PROFIL_CU3="CU3";
	public static final String PROFIL_CU4="CU4";
	
	public static final String MASTER_MC="MC";
	
	public static final String MASTER_ETUDIANT="ME";
	

	public static final String REF_ID_EXPORT_XML = "_refId_export.xml";
	
	public static final String REF_ID_EXPORT_CSV = "_refId_export.csv";
	
	public static final String REF_ID_EXPORT_ALL_CSV = "_allUser.csv";


	public static final String STAISTIC_CATEG_O = "O";
	
	public static final String STAISTIC_CATEG_OU = "OU";

	public static final String CATEG_ETABLISSEMENT = "ETABLISSEMENT";
	
	public static final String CATEG_LABORATOIRE = "LABORATOIRE";

	public static final String ZIP = "exportReferenteil.zip";
	
	public static final String TYPE_DIPLOME_CU2 = "typeDiplomeCU2";
	
	public static final String MASTER_MC1 = "MC0001";
	
	public static final String MASTER_MC2 = "MC0002";
	
	
}
