package com.campus.condorcet.main;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.campus.condorcet.service.ILdapExportService;
import com.campus.condorcet.service.impl.TraitementUtil;


public class ExportReferentiel {
	
	private static final Logger logger = Logger.getLogger(ExportReferentiel.class);
	
	public static void main(String[] args) {
		String date = null;
		String operator=null;
		//java -jar referentielidentite-0.0.1-SNAPSHOT.jar C:/Users/dev/refeIdententite.properties
		//ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		ApplicationContext context = new ClassPathXmlApplicationContext("classpath*:applicationContext.xml");
        ILdapExportService ldapExportService = (ILdapExportService) context.getBean("ldapService");
        ILdapExportService controleAcces = (ILdapExportService) context.getBean("controlAccesService");
       
        Properties properties = new Properties();
        logger.info("++++++ start +++++++");
        try {
        	for(int i=0;i< args.length;i++) {
            	logger.info("arg ["+i+"] = "+ args[i] );
            	/*operator = args[1];
            	date = args[2];*/
            	logger.info("arg ["+i+"] = "+args[i]) ;
            }
        	FileInputStream fis = new FileInputStream(new File(args[0]));
      
        	properties.load(fis);
        }catch(ArrayIndexOutOfBoundsException e) {
        	logger.error("size is : "+args.length);
        }
        catch (IOException e) {
            
            e.printStackTrace();
        }
        logger.info("before requeting  operator is : "+operator+ " date création is : "+date);
        logger.info("------- LOADING EXPORT ALMA ------");
        ldapExportService.loadReferentielUser(properties,"","");
        try {
        	// contrôle si oui ou non on doit executer le contrôle d'accès
        	// Get current date
           // getLastExecutedDate();
        	logger.info("**** LOADING CONTROL ACCESS ***** ");
        	controleAcces.loadReferentielUser(properties, operator, date);
		}catch (Exception e) {
			logger.error("+++++++ ERREOR ++++++"+e);
		}
        
        logger.info("finish to run");
       
       
	}

}
